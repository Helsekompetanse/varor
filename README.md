# Varor #

VARiable OverRide, or "Varor" for short, is a Drupal 7 module that make it more convenient to override host specific variables, for a single site, system wide and for different environments (production, development). Varor is meant as a strictly developer tool only, and is triggered when changing environments (production, development) from Drush. Varor depends on Xautoload and Environment.

### Why Teh Varor? ###

We had immediate need to share properties cross sites on the same server in a secure way. Drupal 7 is by design made to be depended on properties configured in database and there isn't a standardized way of controlling the properties through a file based solution. We also looked for a solution that could ease the transition between development and production hosts. And also, we wanted the solution to be flexible enough to be used in controlled deployment environments running on [Aegir](http://www.aegirproject.org/).

## Who Are We? ##

We are a small Drupal team located in the northern side of the arctic circle in Tromsø, Norway. We deliver e-learning solutions for the public health sector. You can visit us at [helsekompetanse.no](http://helsekompetanse.no/).

## What Is This Repository For? ##

* The main source repository for Varor.

## How Do I Get Set Up? ##

1. Install as regular Drupal module, just make sure the dependencies are met first. Varor depends on [Xautoload](https://www.drupal.org/project/xautoload) and [Environment](https://www.drupal.org/project/environment) module.
2. Create the required configuration files, and decide where and how they should be picked up by Varor.
3. Trigger the changes by switching environment with drush env-switch <your-environment> command.

### Varor Configuration Files ###

The configuration files consist of one main file, and one additional for each environment (production, development..). The environment specific configuration file can be used to override any settings in the main file. A minimum set of configuration files can be created like this.


```
#!shell

$ echo "<?php return array('some_config' => 123);" > varor_conf.php # Required, contains default values.
$ echo "<?php return array('some_config' => 456);" > varor_conf.<your-environment>.php # Replace <your-enviromnent> with development, production, etc.
```

Tip: If you don't want to override any variables, just return an empty array.

The Varor module expect you to have one (shared) varor_conf.php and one varor_conf.<your-environment>.php file for each of your environments. To get a list of available environments, use drush:

```
#!shell

$ drush env-switch
Enter a number.
 [0]  :  Cancel                    
 [1]  :  Production (production)   
 [2]  :  Development (development)
0
```

If the above applies to your environment, you need the following files:

* varor_conf.php
* varor_conf.production.php 
* varor_conf.development.php

If you have a single site, just place these inside your site folder (the same location as your settings.php) and you are done. Varor picks the files up automatically when you use the drush env-switch command and change your environment.

Note, the Varor configuration files location is not limited to your sites configuration folder, more about this in the next section.

### The Configuration File Locations ###

If you run more than one site on your host, you might want to share the configurations between your sites. Fortunately, the Varor module supports that. Here is an explanation in what order Varor look for alternative locations.

1. A path relative to the sites configuration folder, using [conf_path](https://api.drupal.org/api/drupal/includes!bootstrap.inc/function/conf_path/7)().
2. An environment variable, using [getenv](http://php.net/manual/en/function.getenv.php)('**DRUPAL_VAROR_CONF_PATH**').
3. The systems php.ini configuration, using [get_cfg_var](http://php.net/manual/en/function.get-cfg-var.php)('**drupal_varor_conf_path**').

The first valid location discovered will be used, so be aware that location 2 and 3 will not be checked if you have Varor configurations files in your sites configuration folder (1). A location is valid if Varor detects a varor_conf.php and varor_conf.<your-environment>.php in that location. <your-environment> is the the environment your are switching to. The variable overriding happens when changing environments. If none of the locations are valid, Varor does nothing.

You are free to place the configuration files where you like, but some might prefer using the path **/usr/local/etc/drupal_varor** for globally accessible configuration folder on Linux.

## Contribution Guidelines ##

* OOP inspired by Drupal 8 module development practices.
* Unit test coverage for changes and bug fixes should be able to run fast and without heavy bootstrapping, e.g. no hard Drupal dependencies.
* Better documentation and clarity is always welcome.

## Who Do I Talk To? ##

* You can drop us a mail at teknisk at helsekompetanse dot no.

## License ##

[GNU General Public License, version 2](http://www.gnu.org/licenses/old-licenses/gpl-2.0.html).

Copyright (C) 2015 helsekompetanse.no

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.