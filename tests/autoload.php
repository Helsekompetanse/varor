<?php


// Raises 'PHP_Token_Stream' not found when running test coverage if stream not included.
require_once('PHP/Token/Stream/Autoload.php');

// Also depended on php text template.
require_once('Text/Template/Autoload.php');

// Register our own class loader.
spl_autoload_register('my_class_loader');

/*
 * Require all to get a better test coverage rapport.
 * (We still need the autoloader to solve dependencies.)
 */
require_recursively('src');

function my_class_loader($class) {
  $pathern = sprintf("/%s.php$/", end(explode('\\', $class)));
  foreach(dir_to_array('src') as $file) {
    if (preg_match($pathern, $file) === 1) {
      require_once($file);
      // Don't exit loop yet, in case there are duplicate class names.
    }
  }
}

/*
 * Require all php files.
 */
function require_recursively($targetFolder) {
  $all_files = dir_to_array($targetFolder);
  foreach($all_files as $file) {
    switch(strtolower(end(explode('.', $file)))) {
    case 'php':
      require_once($file);
    }
  }
}

/*
 * Return flat array with all path to all files.
 * Based on: http://php.net/manual/en/function.scandir.php#110570
 */
function dir_to_array($dir, $result = array()) {
  $cdir = scandir($dir);
  foreach ($cdir as $key => $value)
  {
    if (!in_array($value,array(".","..")))
    {
      if (is_dir($dir . DIRECTORY_SEPARATOR . $value))
      {
        $result = array_merge($result, dir_to_array($dir . DIRECTORY_SEPARATOR . $value, $result));
      }
      else
      {
        $result[] = $dir . DIRECTORY_SEPARATOR . $value;
      }
    }
  }
  return $result;
}

