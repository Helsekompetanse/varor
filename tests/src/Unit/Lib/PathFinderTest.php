<?php
namespace Drupal\Tests\varor\Unit\Lib;
use Drupal\varor\Lib as s;

class PathFinderTest extends \PHPUnit_Framework_TestCase {

  var $subj;
  function setUp() {
    $this->subj = new s\PathFinder('testenv', new s\DrupalFunctionsDummy());
  }

  /*
   * @covers PathFinder::joinPaths
   */
  function testJoiningOfPathStringsWithAdditionalForwardSlash() {
    $actual = s\PathFinder::joinPaths('/tmp/', '/oleIvars.txt');
    $expected = '/tmp/oleIvars.txt';
    $this->assertEquals($expected, $actual);
  }

  /*
   * @covers PathFinder::joinPaths
   */
  function testJoiningOfPathStringsWithNoForwardSlash() {
    $actual = s\PathFinder::joinPaths('/tmp', 'oleIvars.txt');
    $expected = '/tmp/oleIvars.txt';
    $this->assertEquals($expected, $actual);
  }

  /*
   * @covers PathFinder::getConfigFiles
   */
  function testGetConfigFilesMethod() {
    $expected = array('varor_conf.php', 'varor_conf.testenv.php');
    $actual = $this->subj->getConfigFiles();
    $this->assertEquals($expected, $actual);
  }

  /*
   * @covers PathFinder::locations
   */
  function testLocationsMethodReturnsArrayWithThreeItems() {
    $expected = 3;
    $actual = $this->subj->locations();
    $this->assertCount($expected, $actual);
  }

  /*
   * @covers PathFinder::locations
   */
  function testLocationsMethodReturnsArrayWithThreeKnownKeys() {
    $expected_keys = array(
      'conf path',
      'environment variable',
      'php.ini'
    );
    $actual = $this->subj->locations();
    foreach($expected_keys as $key) {
      $this->assertArrayHasKey($key, $actual);
    }
  }

  /*
   * @covers PathFinder::checkPaths
   */
  function testCheckPathReturnsTrueIfFolderAndFilesExists() {
    $path = 'tests/src/Unit/Lib/PathFinderTest';
    $files = array('varor_conf.php', 'varor_conf.testenv.php');
    $actual = s\PathFinder::checkPath($path, $files);
    $this->assertTrue($actual);
  }

  /*
   * @covers PathFinder::checkPaths
   */
  function testCheckPathReturnsFalseIfFolderExistsButNoFilesProvided() {
    $path = 'tests/src/Unit/Lib/PathFinderTest';
    $files = array();
    $actual = s\PathFinder::checkPath($path, $files);
    $this->assertFalse($actual);
  }

  /*
   * @covers PathFinder::checkPaths
   */
  function testCheckPathReturnsFalseIfFileDoesntExists() {
    $path = 'tests/src/Unit/Lib/PathFinderTest';
    $files = array('varor_conf.php', 'varor_conf.IDONTEXIST.php');
    $actual = s\PathFinder::checkPath($path, $files);
    $this->assertFalse($actual);
  }

  /*
   * @covers PathFinder::checkPaths
   */
  function testCheckPathReturnsFalseIfFolderDoesntExists() {
    $path = dirname(__FILE__) . '/IDONTEXIST';
    $files = array('varor_conf.php', 'varor_conf.testenv.php');
    $actual = s\PathFinder::checkPath($path, $files);
    $this->assertFalse($actual);
  }

  /*
   * @covers PathFinder::findConfigPath
   */
  function testReturnConfigurationDrupalConfPath() {
    $path = 'tests/src/Unit/Lib/PathFinderTest';
    $expected = array('conf path' => $path);
    $actual = $this->subj->findConfigPath($path);
    $this->assertEquals($expected, $actual);
  }

  /*
   * @covers PathFinder::findConfigPath
   */
  function testReturnConfigurationEnvironmentPath() {
    $path = 'tests/src/Unit/Lib/PathFinderTest';
    $expected = array('environment variable' => $path);
    $actual_test_1 = $this->subj->findConfigPath('THISPATHDOESNOTEXIST', $path);
    $this->assertEquals($expected, $actual_test_1);
    // Test with actual environment variable.
    putenv(sprintf('%s=%s', s\PathFinder::CONF_VARIABLE_U, $path));
    $actual_test_2 = $this->subj->findConfigPath('THISPATHDOESNOTEXIST');
    $this->assertEquals($expected, $actual_test_2);
  }

  /*
   * @covers PathFinder::findConfigPath
   */
  function testReturnConfigurationPhpiniPath() {
    $path = 'tests/src/Unit/Lib/PathFinderTest';
    $bogus_path = 'THISPATHDOESNOTEXIST';
    $expected = array('php.ini' => $path);
    $actual = $this->subj->findConfigPath($bogus_path, $bogus_path, $path);
    $this->assertEquals($expected, $actual);
  }
}
