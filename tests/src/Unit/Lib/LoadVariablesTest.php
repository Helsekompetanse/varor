<?php
namespace Drupal\Tests\varor\Unit\Lib;
use Drupal\varor\Lib as s;

class LoadVariablesTest extends \PHPUnit_Framework_TestCase {
  /*
   * @covers LoadVariables::loadVariablesFromDisk
   */
  function testLoadExistingVariablesFromDisk1() {
    $environment = 'testenv';
    $subj = new s\LoadVariables($environment);
    $path = 'tests/src/Unit/Lib/LoadVariablesTest1';
    $expected = array(
      'i_am_from_default_conf' => 'foo',
      'i_am_from_environment_conf' => 'bar',
      'i_am_in_both_and_overriden_in_environment_conf' => 'baz'
    );
    $actual = $subj->loadVariablesFromDisk($path);
    $this->assertEquals($expected, $actual);
  }


  /*
   * @covers LoadVariables::loadVariablesFromDisk
   */
  function testMissingEnvConfFileRaisesError() {
    $subj = new s\LoadVariables('testenv');
    $path = 'tests/src/Unit/Lib/LoadVariablesTest2';
    $this->setExpectedException('RuntimeException');
    $subj->loadVariablesFromDisk($path);
    $this->fail('An expected exception has not been raised.');
  }

  /*
   * @covers LoadVariables::loadVariablesFromDisk
   */
  function testMissingDefaultConfFileRaisesError() {
    $subj = new s\LoadVariables('testenv');
    $path = 'tests/src/Unit/Lib/LoadVariablesTest3';
    $this->setExpectedException('RuntimeException');
    $subj->loadVariablesFromDisk($path);
    $this->fail('An expected exception has not been raised.');
  }

  /*
   * @covers LoadVariables::loadVariablesFromDisk
   */
  function testInaccessiblePathRaisesError() {
    $subj = new s\LoadVariables('testenv');
    $path = 'PATHTHATDOESNTEXIST';
    $this->setExpectedException('RuntimeException');
    $subj->loadVariablesFromDisk($path);
    $this->fail('An expected exception has not been raised.');
  }

  /*
   * @covers LoadVariables::loadVariablesFromDisk
   */
  function testLoadExistingVariablesFromDisk4() {
    $environment = 'testenv';
    $subj = new s\LoadVariables($environment);
    $path = 'tests/src/Unit/Lib/LoadVariablesTest4';
    $expected = array(
      'i_am_from_default_conf' => 123,
      'i_am_from_environment_conf' => array("I am Batman"),
      'i_am_in_both_and_overriden_in_environment_conf' => "I am Robin"
    );
    $actual = $subj->loadVariablesFromDisk($path);
    $this->assertEquals($expected, $actual);
  }
}
