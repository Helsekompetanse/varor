<?php

namespace Drupal\varor\Lib;

/**
 * @codeCoverageIgnore
 */
class DrupalFunctions implements iDrupalFunctions {
  function confPath($require_settings = TRUE, $reset = FALSE) {
    return \conf_path($require_settings, $reset);
  }
}
