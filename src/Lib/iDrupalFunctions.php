<?php

namespace Drupal\varor\Lib;

interface iDrupalFunctions {
  function confPath($require_settings = TRUE, $reset = FALSE);
}
