<?php

namespace Drupal\varor\Lib;

class PathFinder {
  const CONF_VARIABLE_L = "drupal_varor_conf_path";
  const CONF_VARIABLE_U = "DRUPAL_VAROR_CONF_PATH";
  const CONF_FILE_DEFAULT = "varor_conf.php";
  const CONF_FILE_ENVIRONMENT = "varor_conf.%s.php";
  var $environment, $drupal_f;

  function __construct($environment, iDrupalFunctions $drupal_f) {
    $this->environment = $environment;
    $this->drupal_f = $drupal_f;
  }

  /*
   * Returns array with key word description and path to configurations.
   * It first check path from \conf_path(), then environment variable, and
   * finally system php.ini.
   * Empty result if none found.
   *
   * The three variables are usually not required, but are handy for unit tests.
   * $conf_path = Override Drupals \conf_path() (might or might not be available).
   * $env_var = Override DRUPAL_VAROR_CONF_PATH environment variable (might or.. ).
   * $phpini = Override drupal_varor_conf_path php.ini value (might or..).
   */
  function findConfigPath($conf_path=FALSE, $env_var=FALSE, $phpini=FALSE) {
    $result = array();
    $files = $this->getConfigFiles();

    $conf_path = $conf_path !== FALSE ? $conf_path : realpath($this->drupal_f->confPath());
    $env_var = $env_var !== FALSE ? $env_var : getenv(self::CONF_VARIABLE_U);
    $phpini = $phpini !== FALSE ? $phpini : get_cfg_var(self::CONF_VARIABLE_L);

    $locations = self::locations($conf_path, $env_var, $phpini);
    foreach($locations as $desc => $loc) {
      if (self::checkPath($loc, $files)) {
        $result = array($desc => $loc);
        break;
      }
    }
    return $result;
  }

  function getConfigFiles() {
    return array(
      self::CONF_FILE_DEFAULT,
      sprintf(self::CONF_FILE_ENVIRONMENT, $this->environment)
    );
  }

  static function checkPath($path, $files) {
    if (!!$path && file_exists($path) && is_dir($path)) {
      foreach($files as $file) {
        if (!file_exists(self::joinPaths($path, $file))) {
          return FALSE;
        }
      }
      return empty($files) ? FALSE : TRUE;
    }
    return FALSE;
  }

  static function locations($conf_path=FALSE, $env_var=FALSE, $phpini=FALSE) {
    return array (
      'conf path' => $conf_path,
      'environment variable' => $env_var,
      'php.ini' => $phpini
    );
  }

  // Credit and glory: http://stackoverflow.com/a/15575293
  static function joinPaths() {
    $paths = array();
    foreach (func_get_args() as $arg) {
      if ($arg !== '') { $paths[] = $arg; }
    }
    return preg_replace('#/+#','/',join('/', $paths));
  }
}
