<?php

namespace Drupal\varor\Lib;

class LoadVariables {
  var $environment;

  function __construct($environment) {
    $this->environment = $environment;
  }

  function loadVariablesFromDisk($path) {
    if (!$this->checkPath($path)) {
      $msg = sprintf("Folder not accessible or files missing in '%s'", $path);
      throw new \RuntimeException($msg);
    }
    $varor_conf = $this->getVarorConfFile($path);
    $varor_conf_env = $this->getVarorConfEnvFile($path);
    return array_merge(include($varor_conf), include($varor_conf_env));
  }

  protected function checkPath($path) {
    $varor_conf = $this->getVarorConfFile($path);
    $varor_conf_env = $this->getVarorConfEnvFile($path);
    return file_exists($varor_conf) && file_exists($varor_conf_env);
  }

  protected function getVarorConfFile($path) {
    return PathFinder::joinPaths($path, PathFinder::CONF_FILE_DEFAULT);
  }

  protected function getVarorConfEnvFile($path) {
    $file_name = sprintf(PathFinder::CONF_FILE_ENVIRONMENT, $this->environment);
    return PathFinder::joinPaths($path, $file_name);
  }
}
