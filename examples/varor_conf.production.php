<?php

/*
 * The varor_conf.production.php gets loaded after varor_conf.php,
 * and any default/common variables configured in varor_conf.php can be
 * overriden. New variables can also be added as needed.
 *
 * At minimum this file must return an empty array.
 * E.g. return array();
 */

return array(
  // A debug option that is disabled for all production environments.
  'some_debug_option' => FALSE,
);
