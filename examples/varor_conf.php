<?php

/*
 * The varor_conf.php gets loaded first and is useful for
 * setting default/common variables for all environments and instances on the server.
 *
 * At minimum this file must return an empty array.
 * E.g. return array();
 */

return array(
  // Default secret key for all environments.
  'secret_key'     =>   'S3cr3T\KeY3x4mple001!',
  // Another key that is default for all environments.
  'another_key'     =>   'Anoth3R\KeY3x4mple002!',
);
