<?php

/*
 * The varor_conf.development.php gets loaded after varor_conf.php,
 * and any default/common variables configured in varor_conf.php can be
 * overriden. New variables can also be added as needed.
 *
 * At minimum this file must return an empty array.
 * E.g. return array();
 */

return array(
  // A debug option that is enabled for all development environments.
  'some_debug_option' => TRUE,
  // Another key that is default for all environments, but overriden for development.
  'another_key'     =>   'Overridden\Key3xample003!',
);
